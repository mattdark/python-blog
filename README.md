# Personal Blog with Python

A blog built with Python and Flask that lists articles from DEV

Based on [Blog Template](https://codepen.io/michaelgermini/pen/ENvmMy)

DEV API: [docs.forem.com/api](https://docs.forem.com/api/)

Slides: [docs.google.com/presentation/d/13AAiV2Mdiab5nKk8qMitlCZ-BwgwRcin7VWPTuFDv90/edit?usp=sharing](https://docs.google.com/presentation/d/13AAiV2Mdiab5nKk8qMitlCZ-BwgwRcin7VWPTuFDv90/edit?usp=sharing)
